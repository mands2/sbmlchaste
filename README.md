# SBML Chaste Translator

SBMLTranslator is a code translator that takes an [SBML](http://sbml.org) models and converts it into a set of ODEs for use with the [Chaste cell modelling framework](http://www.cs.ox.ac.uk/chaste). It also supports several other backends, namely C and the [Ode](https://bitbucket.org/mands/ode) modelling DSL.
This document details the main usage and information regarding the system.

On this page:

* [Models](#markdown-header-models)
* [Building](#markdown-header-building)
* [Translator Usage](#markdown-header-translator-usage)
* [Chaste Integration](#markdown-header-chaste-integration)
* [Other Backends](#markdown-header-other-backends)
* [Internal Details](#markdown-header-internal-details)
* [License](#markdown-header-license)


## Models
SBMLTranslator uses the official [libSBML](http://sbml.org/Software/libSBML/) library to process models and supports SBML Level 2 and 3 models.
It supports only a subset of SBML functionality at present, however this will be extended in the future as required for integration with Chaste.

The system supports SBML models with a single compartment, containing (reversible) reactions and rate equations. Local and global parameters are supported, however events are not currently supported.
This model of reactions is translated into a flattened form of equations that represent a continuous, deterministic representation of the system using ODEs.

## Building
SBMLTranslator is written in F# and C#, requiring version 4.5 or above of the CLR platform. This can be provided by .NET on Windows, or by Mono 3+ on Linux and OSX. It requires F# 3 or above, obtainable from [GitHub](https://github.com/fsharp/fsharp).
SBMLTranslator is distributed as a Visual Studio 2012 solution that was developed and can be opened in MonoDevelop 4+.

The system requires several libraries, these are distributed within the repository within the [`Libs`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/Libs) directory and are covered by their own respective licenses,

* RazorTemplate -- a standalone template engine used to generate output source code, based on the [MVC Razor engine](http://weblogs.asp.net/scottgu/archive/2010/07/02/introducing-razor.aspx‎)
* libSBML -- the SBML utility library, included as a .NET DLL (a Linux version of the shared library this uses is included in the repository)


## Translator Usage
SBMLTranslator is a command-line program that takes an SBML file as input and performs a source-transform, generating either a C++, C, or _Ode_ simulatable representation. The program takes several options that alter its behaviour, these are displayed when the program is run without options:

~~~
  SBMLTranslator:
  -v: Verbose mode
  -w: Display SBML Warnings
  -f <string>: Input File
  -o <string>: Output File <cpp*, c, ode>
  -m <string>: Chaste Model Type <srn*, ccm, cardiac, none>
  -t <string>: Custom Template
  --help: display this list of options
  -help: display this list of options
~~~

The most important commands are:

* `-o <string>` - sets the output file mode
  * `cpp` (default) generates a single `.hpp` header file that can be used directly within Chaste (see next section)
  * `c` generates a C file that can be compiled and used with other external tools to simulate the model
  * `ode` generates an _Ode_ file for use within the _Ode_ DSL to extend and simulate the model
* `-m <string>` - enables the Chaste integration and sets the model type used within Chaste
  * `srn` (default) generates a sub-cellular-reaction network model
  * `ccm` generates a cell-cycle model
  * `cardiac` not yet supported
  * `none` does not generate any Chaste model
* `-t <string>` - takes a filepath to a custom template, using the Razor template syntax (described [here](www.w3schools.com/aspnet/webpages_razor.asp‎)), that can be used to generate new output formats as required. See [`ViewModelDump.cshtml`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/Template/ViewModelDump.cshtml) for an example.


## Chaste Integration

When the `-m` option is used, SBMLTranslator generates a `.hpp` header file that may be used directly within the Chaste modelling framework.
The exact mix of classes created depends on the option passed to the system, however at the moment sub-cellular reaction networks (SRN) and cell-cycle models (CCM) are supported. The entire model and integration code is placed within the self-contained header file.

This header file consists of 2 main classes and some required boiler-plate code.

* A class inheriting from `AbstractOdeSystem` that implements the model as a system of ODEs. This class is fully code-generated and sets up the system for solution using an explicit solver
* Depending on the model type chosen, either
 * An instantiation of `SbmlSrnWrapperModel` using the above ODE class to generate a new SRN Model;
 * An instantiation of `SbmlCcmWrapperModel` using the above ODE class to generate a new CCM Model;

Both classes, `SbmlSrnWrapperModel` and `SbmlCcmWrapperModel`, are templated over the desired ODE class and size of the system. They have been added to the Chaste framework to aid the reuse of code common to all SBML-derived models within cell-based SRN and CCM models.


### Chase Object Integration

SBMLTranslator supports one more further use-case, generating an differing `AbstractOdeSystem` that, instead of creating the ODE system as C++ source, relies on a shared library dynamically loaded at run-time that implement the model.
This library describes the system using a C-based FFI depicted in the [`OdeModel.h`](https://bitbucket.org/mands/sbmlchaste/src/master/Resources/SBMLTranslator/OdeModel.h) file within this repository, including functions to determine the initial values and to calculate the derivatives of the system during simulation.

This `ObjOdeSystem` is code-generated using the `dl` library on UNIX-like platforms. As a subclass of `AbstractOdeSystem` it can be used with the `SbmlSrnWrapperModel` and `SbmlCcmWrapperModel` classes to generate compatible SRN and CCM models for use in cell-based Chaste.

This output mode is useful to extend the cell-based functionality to use models created by other systems and processes, such as, C and _Ode_ models. It can eventually be extended to support runtime-based LLVM code-generation of models using the same interface. The output is enabled when either the `c` or `ode` output backend (see next section) is selected in conjunction with Chaste integration. 

----

## Other Backends

SBMLTranslator is extensible and supports several other backends using the RazorTemplate engine. This template engine is ideal for use with languages that use a C-style expression notation. To this effect, we have implemented both _Ode_ and C backends. The compilation and integration of these output models is left to the user.

### Ode Backend

[Ode](https://bitbucket.org/mands/ode) is a modelling DSL designed for created modular models based on ODEs, SDEs, or discrete reactions.
SBMLTranslator generates an _Ode_ model file containing a single module that implements the system as a set of ODEs. This can be imported into existing _Ode_ models as required. _Ode_ provides several LLVM-based internal solvers, but also supports compiling the model as a shared library for reuse within existing modelling systems, including Chaste itself. The shared library exposes the same interface depicted by [`OdeModel.h`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/Resources/OdeModel.h).

### C Backend

The C backend generates a standalone C99 source file that implements the model as a set of ODEs. The model uses the same FFI as the _Ode_ exported model depicted by [`OdeModel.h`](https://bitbucket.org/mands/sbmlchaste/src/master/Resources/OdeModel.h).
This can be be imported directly into an existing codebase or again compiled as a shared library for use with external solvers. This compiled library may then be used within Chaste using the `ObjOdeSystem` previously described.

## Internal Details

SBMLTranslator is structured similar to most compilers, with well-defined front-, middle- and back-ends. We utilise the [libSBML](http://sbml.org/Software/libSBML/) library to parse the model, this generate its own internal structure used within front-end. This is converted within [`Converter`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/Converter.fs) module into an intermediate representation (IR) [`ASTModel`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/AST.fs) that can be used more readily within the system. Several functions operate on the model in this form and it could be extended to support further features and model-specific optimisations.

Finally we convert this model IR into a final representation for use within the backend, i.e. to translate into another source format.
This model is termed the [`ViewModel`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/FileOutput.fs), and is passed to several Razor-syntax templates that generate a specific source-code representations of the model as ODEs. Again, this level could be extended to provide further low-level optimisations as required.

----

# License

All source code created as part of the SBMLTranslator project (and related LibSBMLTest project) is released under the MIT license. The license file is located [here](https://bitbucket.org/mands/sbmlchaste/src/master/LICENSE).
All libraries distributed within the repository (in the [`Libs`](https://bitbucket.org/mands/sbmlchaste/src/master/SBMLTranslator/Libs) directory) are included only to ease compilation and are covered by their own respective licenses.


