module FileOutput

open System
open System.IO
open System.Reflection
open RazorEngine
open RazorEngine.Configuration
open RazorEngine.Templating
open RazorEngine.Text

open Utils
open AST

(* Module takes an internal AST and generates a ViewModel that 
* we can pass to Razor Templating engine to generate a C++ file 
*)

/// Expression type, name and C-style expression
type exprs = List<id * string>
/// The ViewModel class
type Model( size : int,                                 // size of the system
            name : string,                              // model name
            compartment : (id * float),                 // model compartment
            stateDefs : List<id * int * float>,         // name, index, initial val
            globalDefs : exprs,                         // global list of expressions
            reactionDefs : List<(id * string * exprs)>, // list of reactions (name, rate expr, local expressions)
            rateEqns : List<id * int * string>          // list of rate equations (name, index, rate expr)
            ) = class

    member x.Size = size
    member x.Name = name
    member x.Compartment = compartment
    member x.StateDefs = stateDefs
    member x.GlobalDefs = globalDefs
    member x.ReactionDefs = reactionDefs
    member x.RateEqns = rateEqns
end

// extraction functions ////////////////////////////////////////////////////////////////////////
/// Create the system initial values
let stateDefs (sbmlAST : ASTModel) = 
    calcInitialValues sbmlAST |> List.mapi (fun i (n,v) -> (n, i, v))

/// Create the parameters in the system
let paramExpr ps = 
    let writeParam = function
        //| ParamConst(n, v) -> (n, sprintf "%.17g" v)
        | ParamConst(n, v) -> (n, sprintf "%g" v)
        | ParamVar(n, s) -> (n, s)

    List.sort ps // sort by datatype order
    |> List.map writeParam

/// Return the list of global parameters
let globalValues (sbmlAST : ASTModel) = paramExpr sbmlAST.globalParams

/// Return the reaction rate equations in the system
let reactions (sbmlAST : ASTModel) = 
    Map.toList sbmlAST.reactions 
    |> List.map (fun (n, r) -> (n, r.rate, paramExpr r.localParams))

/// Calculate and return the ODEs that model the system behaviour based on reaction definitions
let rateEqns (sbmlAST : ASTModel) = 
    let buildReactionSum = function 
        | [] -> "0"
        | xs -> String.concat " + " xs

    // species change rate set as (sum of reactants) - (sum of products)
    let buildRateExpr = function
        | ReactionRate(expr) -> expr
        | ReactionSet(reactionsAsReactant, reactionsAsProduct) -> sprintf "(%s) - (%s)" (buildReactionSum <| Set.toList reactionsAsProduct) 
                                                                                        (buildReactionSum <| Set.toList reactionsAsReactant)
    
    calcStateValues sbmlAST
    |> List.mapi (fun i (n, s) -> (n, i, Option.get s.speciesModifier |> buildRateExpr))


// Module entrypoint //////////////////////////////////////////////////////////////////////////////////
/// Takes the file location and the internal AST model, build the output ViewModel, 
/// applies the required templates (based on cmd-args) and saves the outputted files to disk in the same location
let outputFile (inFilePath : string) (sbmlAST : ASTModel) =
    // build the view model to pass to the template
    let model = new Model(  calcSystemSize sbmlAST,
                            sbmlAST.modelName,
                            sbmlAST.compartment,                            
                            stateDefs sbmlAST,
                            globalValues sbmlAST,
                            reactions sbmlAST,
                            rateEqns sbmlAST)

    // setup the template system
    let config = new TemplateServiceConfiguration();
    config.EncodedStringFactory <- new RawStringFactory();
    // create a new TemplateService with the configuration and set as default template service
    Razor.SetTemplateService(new TemplateService(config)); 

    // write the string to disk
    let writeFile prefix ext data = 
        let outFilePath = Path.GetDirectoryName(inFilePath)
        let outFileName = sprintf "Sbml%s%s.%s" sbmlAST.modelName prefix ext
        vLog <| sprintf "Writing Generated output to - %s" (Path.Combine(outFilePath, outFileName))
        File.WriteAllText(Path.Combine(outFilePath, outFileName), data)

    // apply template to SBML Model and return string
    let applyTemplate templateFile = 
        let exePath = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
        let templateFilePath = Path.Combine(exePath, "Templates", templateFile)
        vLog <| sprintf "Using template at - %s" templateFilePath
        Razor.Parse(File.ReadAllText(templateFilePath), model)

    // read and call the templates
    match !cmdOpts.customTemplate with
    | Some(templatePath) -> applyTemplate templatePath |> writeFile "Custom" "txt" 
    | None -> 
        // generate separate C/Ode model code for separate compilation if needed
        match !cmdOpts.outputMode with
        | C     -> applyTemplate "COdeModel.cshtml" |> writeFile "" "c"
        | Ode   -> applyTemplate "OdeOdeModel.cshtml"  |> writeFile "" "ode"
        | _     -> ()
        
        // build the chaste ode model
        let genChasteOdeSystem (_ : Unit) =
            match !cmdOpts.outputMode with
            | CPP   -> applyTemplate "ChasteOdeCppSystem.cshtml"
            | _     -> applyTemplate "ChasteOdeObjSystem.cshtml"
        
        // build the srn/cellcycle/cardiac model
        match !cmdOpts.modelType with
        | Some(SRN)       -> (genChasteOdeSystem () + applyTemplate "ChasteSrnModel.cshtml") |> writeFile "Model" "hpp"
        | Some(CellCycle) -> (genChasteOdeSystem () + applyTemplate "ChasteCcmModel.cshtml") |> writeFile "Model" "hpp"
        | Some(Cardiac)   -> failwith("Cardiac Models not yet supported");
        | None -> ()
