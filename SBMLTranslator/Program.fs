module Main

open System
open System.IO
open System.Reflection
open libsbmlcs
open Utils
open Args

(* Main module that runs program *)

/// Opens and processes a SBML file taken from the command-line
/// This is the program main method and involves parsing, verifying and translating the model
let openSBMLfile () = 
    let inFilePath = !cmdOpts.inFile
    if File.Exists(inFilePath) then
        vLog <| sprintf "Opening %s" inFilePath

        // determine the model name
        let modelName = Path.GetFileNameWithoutExtension(inFilePath)

        // open & parse the file
        let reader = new SBMLReader()
        let sbmlDoc = reader.readSBMLFromFile(inFilePath)

        // quit on major errors
        if (sbmlDoc.getNumErrors() = 0L) then
            // print warnings
            if (!cmdOpts.warnings = true && sbmlDoc.checkConsistency() <> 0L) then
                sbmlDoc.printErrors()
            try
                // process the model through the translation pipeline
                vLog "Processing valid SBML model"
                sbmlDoc |> Converter.convertAST modelName |> FileOutput.outputFile inFilePath
                ()
            with 
                | SBMLUnsupported(str) -> printfn "(ERROR) Unsupported feature : %s" str
        else
            sbmlDoc.checkConsistency() |> ignore
            sbmlDoc.printErrors()
            failwith("Errors found in SBML Model")

    else
        raise(FileNotFoundException(inFilePath)) 
    ()

// Module Entrypoint ///////////////////////////////////////////////////////////////////////////
/// Main code entrypoint, parses command-line options and sets up the translation process
[<EntryPoint>]
let main args = 
    //printf "Hello world from F#!"
    
    // parse cmd line args
    //vLog <| sprintf "%A" args
    [ ArgInfo("-v", ArgType.Set cmdOpts.verbose, "Verbose mode");
      ArgInfo("-w", ArgType.Set cmdOpts.warnings, "Display SBML Warnings");
      ArgInfo("-f", ArgType.String (fun s -> cmdOpts.inFile := s), "Input File");
      ArgInfo("-o", ArgType.String (function | "c" -> cmdOpts.outputMode := C
                                             // | "llvm" -> cmdOpts.outputMode := LLVM
                                             | "ode" -> cmdOpts.outputMode := Ode
                                             | _ -> cmdOpts.outputMode := CPP), "Output File <cpp*, c, ode>");
      ArgInfo("-m", ArgType.String (function | "cardiac" -> cmdOpts.modelType := Some(Cardiac)
                                             | "ccm" -> cmdOpts.modelType := Some(CellCycle)
                                             | "none" -> cmdOpts.modelType := None
                                             | _ -> cmdOpts.modelType := Some(SRN)), "Chaste Model Type <srn*, ccm, cardiac, none>");
      ArgInfo("-t", ArgType.String (fun s -> cmdOpts.customTemplate := Some(s)), "Custom Template");
    ] |> ArgParser.Parse
    vLog <| sprintf "%A" cmdOpts

    // call main translator
    let res = 
        match !cmdOpts.inFile with
        | "" -> printfn "Usage: CppTranslator --help"; 1
        | _ ->  try 
                    openSBMLfile (); 0
                with
                | Failure(msg) -> printfn "(FAILURE) : %s" msg; 1
                | ex -> printfn "(ERROR) General Exception : %s" (ex.ToString()); 1
    
    // return an integer exit code
    vLog <| sprintf "Done!"
    res
