module Utils
(* General utilities *)
    
    /// mirror of |> operator, similar to $ in Haskell
    let inline (^<|) f a = f a

    /// Custom exceptions
    exception SBMLGeneral of string
    exception SBMLUnsupported of string
    
    /// types to represent the cmd-line options
    type outputMode = CPP | C | Ode // | LLVM
    type modelType = SRN | CellCycle | Cardiac

    /// Type of command line options
    type cmdOpts = {
        verbose: bool ref;
        warnings: bool ref;
        inFile: string ref;
        outputMode: outputMode ref;
        modelType: Option<modelType> ref;
        customTemplate: Option<string> ref;
        }

    /// Record holding command line options set to default values
    let cmdOpts = {
        cmdOpts.verbose = ref false;
        warnings = ref false;
        inFile = ref "";
        outputMode = ref CPP;
        modelType = ref (Some(SRN));
        customTemplate = ref None;
    }

    /// Basic logger used in verbose mode
    let vLog s = if !cmdOpts.verbose then printfn "%s" s
