module AST

(* Modules includes main types that define the AST for the SBML model
 * Will Include 
    * Species
    * Reactions
    * Global Parameters
    * Mini AST syntax for describing expressions (use string for now)
    * Events / Triggers (not yet supported)
 * Notes - 
    * Assumes a single compartment of size 1 with 3 dimensions
*)

// Main AST Types ///////////////////////////////////////////////////////////////////
/// the internal id type used to connect SBML elements
type id = string 
/// holds parameters types from SBML models
type parameter = ParamConst of (id * float) | ParamVar of (id * string)
/// the types of ways in which we can modify species population levels
type speciesModifier = ReactionSet of (Set<id> * Set<id>) | ReactionRate of string

/// Species - holds initial concentration, if constant, and mappings to each reaction it's a part of
type species = {
    initialConc : float;
    constant : bool;
    speciesModifier : Option<speciesModifier>;
}

/// Reaction type - In, Out, Rate, Local params
type reaction = {
    reactants : Set<id>;
    products : Set<id>;
    modifiers : Set<id>;
    rate : string; // embedded C-like expression
    localParams : List<parameter>
    reversible : bool; // we don't really use this atm as rate expression is combined
}

/// Main model, holds name, species, reactions and global params
type ASTModel = { 
    modelName : string;
    compartment : (string * float);
    speciesMap : Map<id, species>;
    reactions : Map<id, reaction>;
    globalParams : List<parameter>
}

// Helper functions that operate on the AST ///////////////////////////////////////////////////////////////////
/// return the (non-constant) species in the model
let calcStateValues (sbmlAST : ASTModel) = 
    Map.toList sbmlAST.speciesMap
    |> List.filter (fun (_, s) -> not s.constant)

/// return the list of initial values of all state vals a tuple of (name * value)
let calcInitialValues (sbmlAST : ASTModel) = 
    calcStateValues sbmlAST
    |> List.map (fun (n, s) -> (n, s.initialConc))

/// return the number of (non-constant) species in the model
let calcSystemSize (sbmlAST : ASTModel) =
    calcStateValues sbmlAST
    |> List.length

/// return the ids of all (non-constant) species in the model
let calcStateIds (sbmlAST : ASTModel) =
    Map.toList sbmlAST.speciesMap
    |> List.mapi (fun i (n, _) -> (n, i))
