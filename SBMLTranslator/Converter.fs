module Converter

open libsbmlcs
open Utils
module A = AST

(* Module takes an libSBML model and converts into an AST from which we can generate code in various languages
 * TODO - switch to using a proper list structure, rather than manual iteration
 * i.e. let convList (l : ListOf) = [ for i in 0L .. (l.size() - 1L) do yield l.get(i)]
*)

// Model Information ///////////////////////////////////////////////////////////////////
/// Print basic info regarding a model
let documentInfo (sbmlDoc : SBMLDocument) = 
    printfn "SBML Document Info"
    printfn "\tLevel: %d" (sbmlDoc.getLevel())
    printfn "\tVersion: %d" (sbmlDoc.getVersion())
    if sbmlDoc.isSetId() then printfn "\tId: %s" (sbmlDoc.getId())

/// Print basic info regarding a model
let modelInfo modelName (model : Model) = 
    printfn "SBML Model Info"
    printfn "\tName: %s" (modelName)
    printfn "\tId: %s" (model.getId())
    printfn "\tCompartments: %d" (model.getNumCompartments())
    printfn "\tSpecies: %d" (model.getNumSpecies())
    printfn "\tReactions: %d" (model.getNumReactions())
    printfn "\tParameters: %d" (model.getNumParameters())
    printfn "\tEvents: %d" (model.getNumEvents())
    printfn "\tRules: %d" (model.getNumRules())
    printfn ""
    printfn "\tFunction Definitions: %d" (model.getNumFunctionDefinitions())
    printfn "\tUnit Definitions: %d" (model.getNumUnitDefinitions())
    printfn "\tCompartment Types: %d" (model.getNumCompartmentTypes())
    printfn "\tSpecies Types: %d" (model.getNumSpeciesTypes())
    printfn "\tInitial Assignments: %d" (model.getNumInitialAssignments())
    printfn "\tConstraints: %d" (model.getNumConstraints())

/// Verify that we support the features in the model
let verifyModel (model : Model) = 
    // events/triggers
    if model.getNumEvents() > 0L then raise(SBMLUnsupported("SBML Events"))
    // compartments
    if model.getNumCompartments() > 1L then raise(SBMLUnsupported("More than 1 compartment"))
    model


// Convert Model //////////////////////////////////////////////////////////////////////////////
/// convert parameters (both global & local)
let convParams (sbmlParamsList : ListOfParameters) : List<A.parameter> =
    let sbmlParams = [ for i in 0L .. (sbmlParamsList.size() - 1L) do yield sbmlParamsList.get(i) ]

    let createParam (p : Parameter) = 
        if p.getConstant() 
            then A.ParamConst (p.getId(), p.getValue())
            else A.ParamVar (p.getId(), "")

    List.map createParam sbmlParams

/// build map of global parameters in the model
let convGlobalParams (model : Model) : List<A.parameter> = 
    let sbmlRules = [ for i in 0L .. (model.getNumRules() - 1L) do yield model.getRule(i) ]

    // for each parameter, if is variable, find the matching rule in the rule list
    let createParamExpr (p : A.parameter) =
        match p with
        | A.ParamConst _ -> p
        | A.ParamVar (i, _) -> 
            // find the expr in the rule list and extract the math
            let r = List.find (fun (r: Rule) -> r.getId() = i) sbmlRules
            A.ParamVar (i, (r.getMath() |> libsbml.formulaToString))

    model.getListOfParameters() |> convParams |> List.map createParamExpr

/// build up structure holding model reactions
let convReactions (model : Model) : Map<A.id, A.reaction> =
    let sbmlReactions = [ for i in 0L .. (model.getNumReactions() - 1L) do yield model.getReaction(i)]

    let printReactionMath (r : Reaction) = 
        if !cmdOpts.verbose then
            let k1 = r.getKineticLaw()
            printfn "Reaction"
            printfn "\tReaction id: %s" <| r.getId()
            printfn "\tReaction reactants: %d" <| r.getNumReactants()
            printfn "\tReaction products: %d" <| r.getNumProducts()
            printfn "\tReaction math: %s" (libsbml.formulaToString <| k1.getMath())
        else ()

    // create the reaction structure
    let createReaction (r : Reaction) = 
        printReactionMath r
        //if r.getReversible() then
        //  raise (SBMLUnsupported(sprintf "Reversible Reaction %s" <| r.getId()))
        
        // assume only one reactant and product for now
        let reactants = Set.ofList [for i in 0L .. (r.getNumReactants() - 1L) do yield r.getReactant(i).getSpecies()]
        let products = Set.ofList [for i in 0L .. (r.getNumProducts() - 1L) do yield r.getProduct(i).getSpecies()]
        let modifiers = Set.ofList [for i in 0L .. (r.getNumModifiers() - 1L) do yield r.getModifier(i).getSpecies()]

        // rate
        let k1 = r.getKineticLaw()
        let rate = k1.getMath() |> libsbml.formulaToString
        // Alt SBML form - ignore for now
        //let sbmlLocalParams = [ for i in 0L .. (k1.getNumLocalParameters() - 1L) do yield k1.getLocalParameter(i)]
        //let param = List.map (fun (p : LocalParameter) -> (p.getId(), p.getValue())) sbmlLocalParams
        let param = convParams <| k1.getListOfParameters()
        { A.reactants = reactants; A.products = products; A.modifiers = modifiers; 
            A.rate = rate; A.localParams = param; A.reversible = r.getReversible() }

    // fold over list of reactions to create the structure
    List.fold (fun m (r : Reaction) -> Map.add (r.getId()) (createReaction r) m) Map.empty sbmlReactions

/// convert the reaction rate rules in the model
let convRateRules (model : Model) : Map<A.id, string> = 
    [ for i in 0L .. (model.getNumRules() - 1L) do yield model.getRule(i)] 
        |> List.filter (fun (r : Rule) -> r.isRate())
        |> List.map (fun (r : Rule) -> (r.getVariable(), r.getMath() |> libsbml.formulaToString))
        |> Map.ofList

/// convert the species - requires reactions and rateRules to create reverse-mapping between species and species modifiers
let convSpecies (model : Model) reactions rateRules : Map<A.id, A.species> = 
    let sbmlSpecies = [ for i in 0L .. (model.getNumSpecies() - 1L) do yield model.getSpecies(i)]
    // create an internal representation of a species from the SBML version
    let createSpecies (s : Species) = 
        // find reactions containing the species
        let findReaction (sR, sP) rId (r : A.reaction) =
            let sR' = if (Set.contains (s.getId()) r.reactants) then (Set.add rId sR) else sR
            let sP' = if (Set.contains (s.getId()) r.products) then (Set.add rId sP) else sP
            (sR', sP')
        let speciesReactions _ = Map.fold findReaction (Set.empty, Set.empty) reactions
        
        // initial value held either in concentration or amount
        let initialConc = max (s.getInitialConcentration()) (s.getInitialAmount())

        // how is this species population modified over time
        let speciesModifier = 
            match (s.getConstant(), Map.tryFind (s.getId()) rateRules) with
            | (true, _) -> None
            | (_, None) -> Some <| A.ReactionSet(speciesReactions ())
            | (_, Some(r)) -> Some <| A.ReactionRate r //Option.map A.ReactionRate r

        // build and return the species structure
        {A.initialConc = initialConc; A.constant = s.getConstant(); A.speciesModifier = speciesModifier }

    // fold over list of Species to create the structure
    List.fold (fun m (s : Species) -> Map.add (s.getId()) (createSpecies s) m) Map.empty sbmlSpecies

/// extracts the name and value of the model compartment
let convCompartment (model : Model) = 
    let (c : Compartment) = model.getCompartment(0L)
    (c.getId(), c.getSize())

/// generate and return the AST model from the SBML model representation
let genAST modelName (model : Model) : A.ASTModel = 
    let reactions = convReactions model
    let rateRules = convRateRules model
    let species = convSpecies model reactions rateRules

    let astModel = {    A.modelName = modelName; 
                        A.compartment = convCompartment model; 
                        A.speciesMap = species;
                        A.reactions = reactions; 
                        A.globalParams = convGlobalParams model 
                    }
    vLog <| sprintf "%A" astModel; astModel


// Module Entrypoint ///////////////////////////////////////////////////////////////////////////
/// Converts an SBML document, as created by libSBML, into the internal AST
let convertAST (modelName : string) (sbmlDoc : SBMLDocument) : A.ASTModel =
    vLog <| "\nConverting SBML model to AST"

    if !cmdOpts.verbose then
        documentInfo sbmlDoc
        modelInfo modelName <| sbmlDoc.getModel()

    // validate the sbml model and convert to AST model
    sbmlDoc.getModel() |> verifyModel |> genAST modelName
