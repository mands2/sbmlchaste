using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libsbmlcs;
using System.IO;


namespace LibSBMLTest
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");

            if (args.Length != 2)
            {
                string myname = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
                Console.WriteLine("Usage: {0} input-filename output-filename", myname);
                Environment.Exit(1);
            }

            string inputFile = args[0];
            string outputFile = args[1];

            if (!File.Exists(inputFile))
            {
                Console.WriteLine("[Error] {0} : No such file.", inputFile);
                Environment.Exit(1);
            }

            SBMLReader reader = new SBMLReader();
            SBMLWriter writer = new SBMLWriter();
            SBMLDocument sbmlDoc = reader.readSBML(inputFile);

            if (sbmlDoc.getNumErrors() > 0)
            {
                sbmlDoc.printErrors();
                Console.WriteLine("[Error] Cannot read {0}", inputFile);
                Environment.Exit(1);
            }

            writer.writeSBML(sbmlDoc, outputFile);

            Console.WriteLine("[OK] Echoed {0} to {1}", inputFile, outputFile);

        }        
    }
}
